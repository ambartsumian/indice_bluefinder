package construccion_base_datos;

import java.io.File;
import java.io.IOException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import org.apache.commons.io.FileUtils;

/**
 * Esta clase nos permite construir un indice MySQL a partir de un archivo
 * resultante de caminos navegacionales, proveniente del algoritmo de busquedas
 * de caminos navegacionale que previamente ha sido ejecutado
 * 
 * @author jlarroque
 *
 */
public class ConstruirIndiceMySQL {

	private static final String SELECT_ID_FROM_PARES = "SELECT id FROM pares WHERE articulo_origen=? AND articulo_destino = ?;";

	private static final String SELECT_ID_FROM_ARTICULOS = "SELECT id FROM articulos WHERE NOMBRE =?;";

	private static final String INSERT_INTO_ARTICULOS_NOMBRE_VALUES = "INSERT INTO articulos (nombre) VALUES (?);";

	private static final String INSERT_INTO_PARES_ARTICULO_ORIGEN_ARTICULO_DESTINO_VALUES = "INSERT INTO pares (articulo_origen,articulo_destino) VALUES (?,?);";

	private static final String INSERT_INTO_CAMINOS_ID_PAR_ARTICULOS_VALOR_CAMINO_VALUES = "INSERT INTO caminos (id_par_articulos,valor_camino) VALUES (?,?);";

	private PreparedStatement preparedStatementInsertVertice;

	private PreparedStatement preparedStatementInsertParDeVertices;

	private PreparedStatement preparedStatementInsertEntreCaminos;

	private PreparedStatement preparedStatementSelectVertice;

	private PreparedStatement preparedStatementSelectParDeVertices;

	public static void main(String[] args) throws Exception {
		if (args.length != 5) {
			System.out
					.println("Uso: ConstruirIndiceMySQL <archivo-con-caminos-navegacionales> <bd> <usuario-bd> <contrasaeña-bd> <delimitador-pares-vertices>");
			System.exit(255);
		}

		// Si los parametros estan bien, continuamos con la construccion de la
		// base MySQL
		new ConstruirIndiceMySQL(args);
	}

	public ConstruirIndiceMySQL(String[] args) {
		super();

		Connection con = null;
		try {

			try {
				Class.forName("com.mysql.cj.jdbc.Driver");
				System.out
						.println("Se pudo cargar el Driver de JDBC Correctamente");
			} catch (ClassNotFoundException e) {
				System.out
						.println("No se puede cargar el driver de mysql jdbc!");
				throw new IllegalStateException(
						"No se pudo cargar el driver JDBC. Revise el parametro -cp para agregar el mismo al classpath",
						e);
			}

			con = DriverManager.getConnection(args[1], args[2], args[3]);

		} catch (SQLException e) {
			// Sino Mostrar error conexion base datos
			System.out
					.println("Error de conexion en base de datos: Revise el nombre, usuario y contraseña de la misma"
							+ ".Codigo de error:"
							+ e.getErrorCode()
							+ ".Mensaje: " + e.getMessage());
		}
		if (con != null) {
			try {
				String delimitadorEnParesDeVertices = args[4];

				// Leemos el archivo de caminos navegacionales pasado por
				// parametro
				List<String> caminosNavegacionales = leerArchivoDesdeDisco(args[0]);

				this.preparedStatementInsertParDeVertices = con
						.prepareStatement(
								INSERT_INTO_PARES_ARTICULO_ORIGEN_ARTICULO_DESTINO_VALUES,
								Statement.RETURN_GENERATED_KEYS);

				this.preparedStatementInsertVertice = con.prepareStatement(
						INSERT_INTO_ARTICULOS_NOMBRE_VALUES,
						Statement.RETURN_GENERATED_KEYS);

				this.preparedStatementSelectVertice = con
						.prepareStatement(SELECT_ID_FROM_ARTICULOS);

				this.preparedStatementSelectParDeVertices = con
						.prepareStatement(SELECT_ID_FROM_PARES);

				// La base de datos esta creada, se continua con el recorrido
				// del
				// archivo
				Iterator<String> iteradorDeLineas = caminosNavegacionales
						.iterator();

				Long idParDeVertices = 0L;
				String lineaDeArchivoDeCaminos;
				while (iteradorDeLineas.hasNext()) {

					lineaDeArchivoDeCaminos = iteradorDeLineas.next();
					// Si es un par de vertices, se insertan los vertices y el
					// par
					if (esParDeVertices(lineaDeArchivoDeCaminos,
							delimitadorEnParesDeVertices)) {
						idParDeVertices = crearORecuperarParDeVerticesEnBD(
								lineaDeArchivoDeCaminos
										.split(delimitadorEnParesDeVertices),
								con);

					} // Si es un camino navegacional, se inserta dicho camino
						// navegacional
					else {

						this.preparedStatementInsertEntreCaminos = con
								.prepareStatement(
										INSERT_INTO_CAMINOS_ID_PAR_ARTICULOS_VALOR_CAMINO_VALUES,
										Statement.RETURN_GENERATED_KEYS);

						// Deshabilitamos auto commit para hacer varios
						// simultaneos
						con.setAutoCommit(Boolean.FALSE);

						// Recolectamos todos los caminos hasta que aparezca el
						// proximo par de vertices, o se termine el archivo, lo
						// que suceda primero
						while (iteradorDeLineas.hasNext()
								&& !esParDeVertices(lineaDeArchivoDeCaminos,
										delimitadorEnParesDeVertices)) {
							// Creamos el camino a partir de UN prepared
							// statement
							crearCaminoNavegacional(idParDeVertices,
									lineaDeArchivoDeCaminos, con);
							// Agregamos al batch el insert
							preparedStatementInsertEntreCaminos.addBatch();

							lineaDeArchivoDeCaminos = iteradorDeLineas.next();
						}
						// Si la ultima linea del batch es el final del archivo,
						// la agregamos al ultimo batch para no perder esta
						// linea
						if (!iteradorDeLineas.hasNext()) {
							crearCaminoNavegacional(idParDeVertices,
									lineaDeArchivoDeCaminos, con);
							preparedStatementInsertEntreCaminos.addBatch();
						}

						preparedStatementInsertEntreCaminos.executeBatch();
						con.commit();
						con.setAutoCommit(Boolean.TRUE);

						// Si la ultima linea del batch es un par de vertices lo
						// agregamos a la tabla correspondiente
						if (esParDeVertices(lineaDeArchivoDeCaminos,
								delimitadorEnParesDeVertices)) {
							idParDeVertices = crearORecuperarParDeVerticesEnBD(
									lineaDeArchivoDeCaminos
											.split(delimitadorEnParesDeVertices),
									con);
						}

					}

				}
			} catch (Exception e) {
				System.out.println("Error inesperado" + e.getStackTrace());
			}
		}

	}

	private void crearCaminoNavegacional(Long idParDeVertices,
			String lineaDeArchivoDeCaminos, Connection con) throws SQLException {
		PreparedStatement pstmt = this.getPreparedStatementInsertEntreCaminos();
		pstmt.setLong(1, idParDeVertices);
		pstmt.setString(2, lineaDeArchivoDeCaminos);
	}

	private Long crearORecuperarParDeVerticesEnBD(String[] parDeVertices,
			Connection con) throws SQLException {

		Long idVerticeDestino;
		Long idVerticeOrigen;

		String verticeOrigen = parDeVertices[0];
		String verticeDestino = parDeVertices[1];

		Long idParDeVertices;

		idVerticeOrigen = crearORecuperarVertice(con, verticeOrigen);
		idVerticeDestino = crearORecuperarVertice(con, verticeDestino);

		idParDeVertices = existeParDeVerticesEnBD(idVerticeOrigen,
				idVerticeDestino, con);
		// Si existe el par lo recuperamos, sino lo creamos
		if (idParDeVertices == 0) {
			idParDeVertices = insertarParDeVerticesEnBd(idVerticeOrigen,
					idVerticeDestino, con);
		}

		return idParDeVertices;
	}

	/**
	 * Crea un articulo en caso de que no exista, lo recupera en caso de que
	 * exista
	 * 
	 * @param st
	 * @param verticeOrigen
	 * @return
	 * @throws SQLException
	 */
	private Long crearORecuperarVertice(Connection con, String verticeOrigen)
			throws SQLException {
		Long idVerticeOrigen = existeVerticeEnBD(verticeOrigen, con);
		if (idVerticeOrigen == 0) {
			idVerticeOrigen = insertarVerticeEnBd(verticeOrigen, con);
		}
		return idVerticeOrigen;
	}

	/**
	 * Inserta par de vertices en Bd. El par no existe en la base.
	 * 
	 * @param idVerticeOrigen
	 * @param idVerticeDestino
	 * @param con
	 * @return
	 * @throws SQLException
	 */
	private Long insertarParDeVerticesEnBd(Long idVerticeOrigen,
			Long idVerticeDestino, Connection con) throws SQLException {
		PreparedStatement pstmt = getPreparedStatementInsertParDeVertices();
		pstmt.setLong(1, idVerticeOrigen);
		pstmt.setLong(2, idVerticeDestino);
		pstmt.execute();
		ResultSet rs = pstmt.getGeneratedKeys();
		rs.next();
		return rs.getLong(1);
	}

	/**
	 * Inserta un vertice en la base. El vertice no existe.
	 * 
	 * @param nombreVertice
	 * @param con
	 * @return
	 * @throws SQLException
	 */
	private Long insertarVerticeEnBd(String nombreVertice, Connection con)
			throws SQLException {
		PreparedStatement pstmt = getPreparedStatementInsertVertice();
		pstmt.setString(1, nombreVertice);
		pstmt.execute();
		ResultSet rs = pstmt.getGeneratedKeys();
		rs.next();
		return rs.getLong(1);
	}

	/**
	 * Control para saber si existe el vertice en la base. Si existe retorna el
	 * ID, sino retorna 0.
	 * 
	 * @param nombreVertice
	 * @param con
	 * @return
	 * @throws SQLException
	 */
	private Long existeVerticeEnBD(String nombreVertice, Connection con)
			throws SQLException {
		PreparedStatement prepareStatement = getPreparedStatementSelectVertice();
		prepareStatement.setString(1, nombreVertice);
		ResultSet select = (ResultSet) prepareStatement.executeQuery();
		return select.next() ? select.getLong(1) : 0L;

	}

	/**
	 * Control para saber si existe el par de vertices en la base. Si existe
	 * retorna el ID, sino retorna 0.
	 * 
	 * @param idVerticeOrigen
	 * @param idVerticeDestino
	 * @param con
	 * @return
	 * @throws SQLException
	 */
	private Long existeParDeVerticesEnBD(Long idVerticeOrigen,
			Long idVerticeDestino, Connection con) throws SQLException {
		PreparedStatement prepareStatement = getPreparedStatementSelectParDeVertices();
		prepareStatement.setLong(1, idVerticeOrigen);
		prepareStatement.setLong(2, idVerticeDestino);
		ResultSet select = (ResultSet) prepareStatement.executeQuery();
		return select.next() ? select.getLong(1) : 0L;
	}

	/**
	 * Controla si la linea indica un par de vertices. Una linea representa un
	 * par de vertices o un camino navegacional.
	 * 
	 * @param lineaDeArchivoDeCaminos
	 * @param separadorEnParesDeVertices
	 * @return
	 */
	private boolean esParDeVertices(String lineaDeArchivoDeCaminos,
			String separadorEnParesDeVertices) {
		return lineaDeArchivoDeCaminos.contains(separadorEnParesDeVertices);
	}

	@SuppressWarnings("unchecked")
	public static List<String> leerArchivoDesdeDisco(String direccionDelArchivo) {
		List<String> archivoLeido = new ArrayList<String>();
		System.out.println("El archivo " + direccionDelArchivo
				+ " existe, se lo abre para su lectura");
		try {
			archivoLeido = (List<String>) FileUtils.readLines(new File(
					direccionDelArchivo));
		} catch (IOException e) {

			System.out.println("El archivo " + direccionDelArchivo
					+ " no existe en el FS " + e.toString());
		}

		System.out.println("Fin de lectura del archivo " + direccionDelArchivo);

		return archivoLeido;
	}

	public PreparedStatement getPreparedStatementInsertEntreCaminos() {
		return preparedStatementInsertEntreCaminos;
	}

	public PreparedStatement getPreparedStatementInsertParDeVertices() {
		return preparedStatementInsertParDeVertices;
	}

	public PreparedStatement getPreparedStatementInsertVertice() {
		return preparedStatementInsertVertice;
	}

	public PreparedStatement getPreparedStatementSelectVertice() {
		return preparedStatementSelectVertice;
	}

	public PreparedStatement getPreparedStatementSelectParDeVertices() {
		return preparedStatementSelectParDeVertices;
	}
}
