-- phpMyAdmin SQL Dump
-- version 4.0.10deb1
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Feb 25, 2017 at 02:47 PM
-- Server version: 5.5.54-0ubuntu0.14.04.1
-- PHP Version: 5.5.9-1ubuntu4.21

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `bluefinderdb`
--
CREATE DATABASE IF NOT EXISTS `bluefinderdb` DEFAULT CHARACTER SET latin1 COLLATE latin1_swedish_ci;
USE `bluefinderdb`;

-- --------------------------------------------------------

--
-- Table structure for table `articulos`
--

CREATE TABLE IF NOT EXISTS `articulos` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `nombre` varchar(500) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `id` (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 COMMENT='Esta tabla contiene todos los articulos o categorias de los pares de vertices' AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `caminos`
--

CREATE TABLE IF NOT EXISTS `caminos` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_par_articulos` int(11) NOT NULL,
  `valor_camino` varchar(1000) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `id_par_articulos` (`id_par_articulos`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `pares`
--

CREATE TABLE IF NOT EXISTS `pares` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `articulo_origen` int(11) NOT NULL,
  `articulo_destino` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `articulo_origen` (`articulo_origen`),
  KEY `articulo_destino` (`articulo_destino`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 COMMENT='Tabla con todos los pares para los que existen caminos navegacionales' AUTO_INCREMENT=1 ;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `caminos`
--
ALTER TABLE `caminos`
  ADD CONSTRAINT `fk_par_articulos` FOREIGN KEY (`id_par_articulos`) REFERENCES `pares` (`id`);

--
-- Constraints for table `pares`
--
ALTER TABLE `pares`
  ADD CONSTRAINT `fk_destino_articulo` FOREIGN KEY (`articulo_destino`) REFERENCES `articulos` (`id`),
  ADD CONSTRAINT `fk_origen_articulo` FOREIGN KEY (`articulo_origen`) REFERENCES `articulos` (`id`);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
